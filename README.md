# Template Molotov

Un site WordPress basé sur [Bedrock](https://roots.io/bedrock/) et incluant tous les plugins
et thèmes utilisés par la coopérative Molotov pour la réalisation de ses sites.

## Prérequis

* PHP >= 7.1
* [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
* [WP-Cli](https://wp-cli.org/fr/)
* [Yarn](https://getyarn.io/)

## Installation

1. `composer install`
2. Mettre à jour les variables dans le fichier `.env`:
  * Base de données
    * `DB_NAME` - Nom de la BD
    * `DB_USER` - Utilisateur de la BD
    * `DB_PASSWORD` - Mot de passe de la BD
    * `DB_HOST` - Hôte de la BD
  * `WP_ENV` - Environnement (`development`, `staging`, `production`)
  * `WP_HOME` - URL du site (https://example.com)
  * `WP_SITEURL` - URL du site avec dossier, le cas échéant (https://example.com/wp)
  * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`
    * Générer avec [wp-cli-dotenv-command](https://github.com/aaemnnosttv/wp-cli-dotenv-command)
3. `wp core install --url=WP_HOME --title="TITRE DU SITE" \
  --admin_user=molotov --admin_email=tech@molotov.ca --admin_password=molotov --skip-email` \
  ou prendre les données sur le staging
4. `wp language core install fr_FR --activate`
5. `wp plugin activate --all`
6. `wp theme activate wp-theme-molotov`
7. `wp rewrite structure '/%postname%/'`
8. `yarn` sous `web\app\themes\wp-theme-molotov`
9. Copier `wpackio.server.js.example` en `wpackio.server.js` et ajuster les valeurs.

## Développement

- `wp server` à la racine
- `yarn start` dans `web/app/themes/molotov`

## Déploiement

- `yarn build` dans `web/app/themes/molotov`
